module WindowModel

using Assets
using Gtk4, GtkObservables, Gtk4.GLib

export Window
mutable struct Window
    win::GtkWindow
    label::GtkObservables.Label
    btn_list::Dict{ Char, Ref{ GtkButton } }
    css_provider::GtkCssProvider
    eck_list::Dict{ String, GtkEventControllerKeyLeaf }
    signal_list::Dict{ String, UInt64 }
end

end
