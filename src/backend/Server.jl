module Server

using Sockets, Base.Threads, JSON, Random

export ASK_LOBBY, START_GAME, WRITE_WORD, WAIT_WORD, WORD_RECEIVED, START_ROUND,
       END_ROUND, END_GAME
@enum EventType begin
    ASK_LOBBY
    START_GAME
    WRITE_WORD
    WAIT_WORD
    WORD_RECEIVED
    START_ROUND
    END_ROUND
    PARTIAL_END_GAME
    END_GAME
end

export Status, LOBBY, WRITTING, WAITING, FINISHED, ENDED
@enum Status begin
    LOBBY
    STARTED
    FINISHED
    ENDED
end

export Guest
mutable struct Guest
    id::UInt64
    name::String
    ready::Bool
end

export HangmanServer
mutable struct HangmanServer
    port::Int64
    server::Sockets.TCPServer
    password::String
    active_connections::Atomic{ Int64 }
    status::Status
    max_guests::Int64
    guests::Dict{ UInt64, Guest }
    events::Dict{ EventType, Event }
    current_turn::UInt64
    current_word::String
    turns_played::Int64
    points::Dict{ UInt64, Vector{ Int64 } }
    end_turn_channel::Channel
    end_game_channel::Channel
    turn_order::Vector{ UInt64 }
end

function is_finished( server::HangmanServer )::Bool
    return server.status == FINISHED || server.status == ENDED
end

function send_message( conn::TCPSocket, message::Any )::Nothing
    print( "Server send: ", JSON.json( message ), "\n" )
    write( conn, JSON.json( message ) * "\n" )
    return
end

function read_message( conn::TCPSocket )::Dict{ String, Any }
    line = replace( readline( conn ), "\n" => "" )
    print( "Server received: ", line, "\n" )
    return JSON.parse( line; inttype = UInt64 )
end

function get_guest_info!( server::HangmanServer, conn::TCPSocket )::UInt64
    message = read_message( conn )
    print( "Message: ", message, "\n" )
    guest = Guest(
                    message[ "id" ],
                    message[ "name" ],
                    message[ "ready" ]
                )
    print( guest )
    server.guests[ guest.id ] =  guest
    return guest.id
end

function increase_active_connections!( server::HangmanServer )::Nothing
    atomic_add!( server.active_connections, 1 )
    return
end

function decrease_active_connection!( server::HangmanServer )::Nothing
    atomic_sub!( server.active_connections, 1 )
    return
end

function setup_events( server::HangmanServer )::Nothing
    for event_type in instances( EventType )
        server.events[ event_type ] = Event()
    end
    return
end

function game_routine( server::HangmanServer, conn::TCPSocket )::Nothing
    print( "Getting guest info\n" )
    id = get_guest_info!( server, conn )
    server.points[ id ] = Vector{ Int64 }()
    print( id, "\n" )
    print( server.guests )

    errormonitor( @async listen_requests( server, conn, id ) )

    while !is_finished( server )
        print( server.status )
        wait( server.events[ START_GAME ] )
        send_message( conn, (
                                status = server.status,
                                event = ASK_LOBBY,
                                guests = server.guests
                            ) )
        if id == server.current_turn
            send_message( conn, (
                                status = server.status,
                                event = WRITE_WORD
                            ) )
            wait( server.events[ WORD_RECEIVED ] )
            reset( server.events[ WORD_RECEIVED ] )
            reset( server.events[ END_ROUND ] )
            notify( server.events[ START_ROUND ] )
            server.turns_played += 1
            wait( server.events[ END_ROUND ] )
            send_message( conn, (
                                status = server.status,
                                event = END_ROUND,
                                points = server.points
                            ) )
        else
            send_message( conn, (
                                status = server.status,
                                event = WAIT_WORD
                            ) )
            wait( server.events[ START_ROUND ] )
            send_message( conn, (
                                status = server.status,
                                event = START_ROUND,
                                word = server.current_word
                            ) )
            wait( server.events[ END_ROUND ] )
            send_message( conn, (
                                status = server.status,
                                event = END_ROUND,
                                points = server.points
                            ) )
        end
        sleep(5)
    end
    print( "ENDING GAME", id )
    send_message( conn, (
                            status = server.status,
                            event = END_GAME
                        ) )
    
    return
end

function check_end_turn( server::HangmanServer )::Nothing
    players_finished = 1
    turns = 0
    while server.status != is_finished( server )
        take!( server.end_turn_channel )
        players_finished += 1
        turns += 1
        if players_finished == length( server.guests )
            if isempty( server.turn_order )
                server.status = FINISHED
                reset( server.events[ START_ROUND ] )
                notify( server.events[ END_ROUND ] )
            else
                server.current_turn = pop!( server.turn_order )
                reset( server.events[ START_ROUND ] )
                notify( server.events[ END_ROUND ] )
                players_finished = 1
            end
        end
    end
    return
end

function check_end_game( server::HangmanServer )::Nothing
    num_closes = 0
    wait( server.events[ START_GAME ] )
    while num_closes < length( server.guests )
        take!( server.end_game_channel )
        num_closes += 1
        print( num_closes, length( server.guests ) )
        if num_closes == length( server.guests )
            server.status = ENDED
        end
    end
    print( "NOTIFY ENDGAME" )
    notify( server.events[ END_GAME ] )
    return
end

function listen_requests( server::HangmanServer, conn::TCPSocket, id::UInt64 )
    while server.status != ENDED
        print( "Listening requests\n" )
        message = read_message( conn )
        print( "Listen requests received: ", message, "\n" )
        if message[ "event" ] == string( WORD_RECEIVED )
            server.current_word = message[ "word" ]
            notify( server.events[ WORD_RECEIVED ] )
        elseif message[ "event" ] == string( END_ROUND )
            push!( server.points[ id ], message[ "points" ] )
            put!( server.end_turn_channel, 1 )
        elseif message[ "event" ] == string( ASK_LOBBY )
            send_message( conn, (
                                    status = server.status,
                                    event = ASK_LOBBY,
                                    guests = server.guests
                                ) )
        elseif message[ "event" ] == string( END_GAME )
            put!( server.end_game_channel, 1 )
            wait( server.events[ END_GAME ] )
        end
    end
end

export start_server_game
function start_server_game( server::HangmanServer )::Nothing
    for _ in 1:2
        turn_aux = Vector{ UInt64 }()
        for ( id, guest ) in server.guests
            push!( turn_aux, id )
        end
        shuffle!( turn_aux )
        server.turn_order = vcat( server.turn_order, turn_aux )
    end
    
    server.current_turn = pop!( server.turn_order )

    notify( server.events[ START_GAME ] )
    return
end

export start_server
function start_server( server::HangmanServer )::Nothing
    server.server = listen( server.port )
    setup_events( server )
    errormonitor( @async check_end_turn( server ) )
    errormonitor( @async check_end_game( server ) )
    while server.active_connections[] < server.max_guests
        conn = accept( server.server )
        print( "\nClient Connected\n" )
        increase_active_connections!( server )
        errormonitor( @async begin
            print( "Beggining game routine\n" )
            game_routine( server, conn )
            wait( server.events[ END_GAME ] )
            print( "GAME ENDED" )
            close( conn )
            decrease_active_connection!( server )
        end )
    end
    return
end

end
