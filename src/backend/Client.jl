module Client

using ..Hangman.Server
using ..Hangman.PageConstants
using Sockets, Base.Threads, JSON

export HangmanClient
mutable struct HangmanClient
    id::UInt64
    name::String
    ip::Sockets.IPv4
    port::Int64
    password::String
    status::Status
    conn::TCPSocket
    guests::Dict{ UInt64, Guest }
    current_word::String
    points::Dict{ UInt64, Vector{ Int64 } }
end

function construct_guests_dict( json::Dict{ String, Any } )
    guests = Dict{ UInt64, Guest }()
    for ( id, guest ) in json
        guests[ parse( UInt64, id ) ] = Guest(   
                                                guest[ "id" ],
                                                guest[ "name" ],
                                                guest[ "ready" ]
                                            )
    end
    return guests
end

function construct_points_dict( json::Dict{ String, Any } )
    points = Dict{ UInt64, Vector{ Int64 } }()
    for ( id, scores ) in json
        points[ parse( UInt64, id ) ] = Vector{ Int64 }()
        for point in scores
            push!( points[ parse( UInt64, id ) ], point )
        end
    end
    return points
end

function send_message( conn::TCPSocket, message::Any )::Nothing
    print( "Client send: ", JSON.json( message ), "\n" )
    write( conn, JSON.json( message ) * "\n" )
    return
end

function read_message( conn::TCPSocket )::Dict{ String, Any }
    line = replace( readline( conn ), "\n" => "" )
    print( "Client received: ", line, "\n" )
    return JSON.parse( line; inttype = UInt64 )
end

function send_guest_info( client::HangmanClient )::Nothing
    guest = (
                id = client.id,
                name = client.name,
                ready = false
            )
    print( guest, "\n" )
    send_message( client.conn, guest )
    return
end

function client_game_routine( client::HangmanClient )::Nothing
    send_guest_info( client )

    while client.status != FINISHED && client.status != ENDED
        print( "Client reading messages..." )
        message = read_message( client.conn )
        print( "Client receives: ", message, "\n" )
        if message[ "event" ] == string( WRITE_WORD )
            set_current_page( WRITE_ONLINE_WORD_PAGE )
        elseif message[ "event" ] == string( WAIT_WORD )
            set_current_page( WAIT_WORD_PAGE )
        elseif message[ "event" ] == string( START_ROUND )
            client.current_word = message[ "word" ]
            set_current_page( HANGMAN_ONLINE_PAGE )
        elseif message[ "event" ]  == string( END_ROUND )
            client.points = construct_points_dict( message[ "points" ] )
            set_current_page( END_ROUND_PAGE )
        elseif message[ "event" ] == string( END_GAME )
            set_current_page( END_GAME_PAGE )
            client.status = FINISHED
            send_message( client.conn, (
                                            event = END_GAME,
                                       ) )
        elseif message[ "event" ] == string( ASK_LOBBY )
            client.guests = construct_guests_dict( message[ "guests" ] )
        end
    end

    return
end

export request_guests
function request_guests( conn::TCPSocket )::Nothing
    send_message( conn, ( event = ASK_LOBBY, ) )
    return
end

export send_word
function send_word( client::HangmanClient, word::String )::Nothing
    send_message( client.conn, ( word = word, event = WORD_RECEIVED ) )
    return
end

export send_points
function send_points( client::HangmanClient, points::Int64 )::Nothing
    send_message( client.conn, ( event = END_ROUND, points = points ) )
    return
end

export start_client
function start_client( client::HangmanClient )::Nothing
    print( "Client connecting... " * string( client.ip ) * ":" * string( client.port ) )
    client.conn = connect( client.ip, client.port )
    print( "Client connected" )
    client_game_routine( client )
    return
end

end
