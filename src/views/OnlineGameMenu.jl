module OnlineGameMenu

using ..Hangman, ..Hangman.WindowModel
using ..Hangman.UI

using Gtk4, GtkObservables, Gtk4.GLib

export create_online_game_options_menu
function create_online_game_options_menu( win::Window )::Nothing
    box = GtkBox(:v)
    box.homogeneous = false
    
    banner = GtkBox(:v)
    change_css_class(win, banner, "banner")
   
    buttons_box_create = GtkCenterBox()

    create_room_btn = GtkButton( "Create Room" )
    create_room_btn.action_name = "online_game_menu.create_room_page"
    create_room_btn.action_target = GVariant( "a" )
    change_css_class(win, create_room_btn, "start-btn")

    buttons_box_create[ :center ] = create_room_btn

    buttons_box_join = GtkCenterBox()

    join_room_btn = GtkButton( "Join Room" )
    join_room_btn.action_name = "online_game_menu.join_room_page"
    join_room_btn.action_target = GVariant( "a" )
    change_css_class(win, join_room_btn, "start-btn")

    buttons_box_join[ :center ] = join_room_btn
   
    push!( box, banner )
    push!( box, buttons_box_create )
    push!( box, buttons_box_join )
    push!( win.win, box )
    change_css_class( win, win.win, "menu" )
    return
end

end
