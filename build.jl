using PackageCompiler
create_app(".", "../hangman_compiled", force=true, incremental=true, sysimage_build_args=`--compile=min -O0 --min-optlevel=0 --inline=no --check-bounds=no`)

using Assets, Hangman
copy_assets(Hangman, "../hangman_compiled")

