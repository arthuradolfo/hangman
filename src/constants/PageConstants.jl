module PageConstants

export Pages
export MAIN_MENU, LOCAL_GAME_MENU, ONLINE_GAME_MENU, LOBBY_PAGE, WRITE_WORD_PAGE,
       WRITE_ONLINE_WORD_PAGE, WAIT_WORD_PAGE, END_ROUND_PAGE, END_GAME_PAGE,
       CREATE_ROOM_PAGE, JOIN_ROOM_PAGE, HANGMAN_PAGE, HANGMAN_ONLINE_PAGE,
       HANGMAN_WIN_PAGE, HANGMAN_LOSE_PAGE, HANGMAN_LOST_PAGE, HIGHSCORE_PAGE
@enum Pages begin
    MAIN_MENU
    LOCAL_GAME_MENU
    ONLINE_GAME_MENU
    LOBBY_PAGE
    WRITE_WORD_PAGE
    WRITE_ONLINE_WORD_PAGE
    WAIT_WORD_PAGE
    END_ROUND_PAGE
    END_GAME_PAGE
    CREATE_ROOM_PAGE
    JOIN_ROOM_PAGE
    HANGMAN_PAGE
    HANGMAN_ONLINE_PAGE
    HANGMAN_WIN_PAGE
    HANGMAN_LOSE_PAGE
    HANGMAN_LOST_PAGE
    HIGHSCORE_PAGE
end

last_page = Ref{ Pages }()
current_page = Ref{ Pages }()

export set_current_page
function set_current_page( page::Pages )::Nothing
    current_page[] = page
    return
end

export get_current_page
function get_current_page()::Pages
    return current_page[]
end

export set_last_page
function set_last_page( page::Pages )::Nothing
    last_page[] = page
    return
end

export get_last_page
function get_last_page()::Pages
    return last_page[]
end

end
