module PropertiesModel

export MAX_MISTAKES 
MAX_MISTAKES = 5 

export ONE_PLAYER, TWO_PLAYERS, ONLINE_PLAYER
@enum GameMode begin
    ONE_PLAYER
    TWO_PLAYERS
    ONLINE_PLAYER
end

export Properties
mutable struct Properties
    dictionaire::Vector{String}
    current_word::String
    lifes::Int64
    mistakes::Int64
    score::Int64
    mode::GameMode
    keys_pressed::Vector{Char}
end

export reset_lifes
function reset_lifes( props::Properties )::Nothing
    props.lifes = 5
    return
end

export reset_keys_pressed
function reset_keys_pressed( props::Properties )::Nothing
    props.keys_pressed = Vector{Char}()
    return
end

export decrease_lifes
function decrease_lifes( props::Properties )::Nothing
    props.lifes -= 1
    return
end

export increase_lifes
function increase_lifes( props::Properties )::Nothing
    props.lifes += 1
    return
end

export decrease_mistakes
function decrease_mistakes( props::Properties )::Nothing
    props.mistakes -= 1
    return
end

export increase_mistakes
function increase_mistakes( props::Properties )::Nothing
    props.mistakes += 1
    return
end

export reset_mistakes
function reset_mistakes( props::Properties )::Nothing
    props.mistakes = 0
    return
end

export add_correct_word_score
function add_correct_word_score( props::Properties )::Nothing
    props.score += 100
    return
end

export set_mode
function set_mode( props::Properties, mode::GameMode )::Nothing
    props.mode = mode
    return
end

end
