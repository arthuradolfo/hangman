module CreateRoomPageActions

using ..Hangman
using ..Hangman.Server, ..Hangman.Client
using ..Hangman.PageConstants
using ..Hangman.WindowModel, ..Hangman.PropertiesModel
using ..Hangman.CreateRoomPage
using ..Hangman.Words, ..Hangman.PageConstants
using ..Hangman.UI

using Gtk4, GtkObservables, Gtk4.GLib, Base.Threads, Sockets

function reveal_password_cb( a, v )::Nothing
    for ( i, child ) in enumerate( get_window().win[] )
        if i == 3
            child[ :center ].visible = v[ Bool ]
        end
    end
    Gtk4.GLib.set_state( a, v )
    return
end

function create_room_cb( a, v )::Nothing
    set_password = false
    name = ""
    password = ""
    max_guests = 2

    for ( i, child ) in enumerate( get_window().win[] )
        if i == 2
            name = child[ :center ].text
        end
        if i == 3
            set_password = child[ :center ].active
        end
        if i == 4 && set_password
            password = child[ :center ].text
        end
        if i == 5
            max_guests = child[ :center ].active + 2
        end
    end

    set_server( HangmanServer(
                    5555,
                    Sockets.TCPServer(),
                    password,
                    Atomic{ Int64 }( 0 ),
                    LOBBY,
                    max_guests,
                    Dict{ UInt64, Guest }(),
                    Dict{ EventType, Event }(),
                    0,
                    "0",
                    0,
                    Dict{ UInt64, Vector{ Int64 } }(),
                    Channel(),
                    Channel(),
                    Vector{ Int64 }()
                )
            )

    errormonitor( @async start_server( get_server() ) )
    
    print( "Server started." )

    set_client( HangmanClient(
                    rand( UInt64 ),
                    name,
                    Sockets.localhost,
                    5555,
                    password,
                    LOBBY,
                    TCPSocket(),
                    Dict{ UInt64, Guest }(),
                    "",
                    Dict{ UInt64, Vector{ Int64 } }()
            ) )

    print( "Client id: ", get_client().id, "\n" )

    errormonitor( @async start_client( get_client() ) )


    set_current_page( LOBBY_PAGE )
    Gtk4.GLib.set_state( a, v )
    return
end


export add_create_room_actions
function add_create_room_actions( win::Window )::Nothing
    action_group = GSimpleActionGroup()
    add_stateful_action( GActionMap( action_group ), "reveal_password", false, reveal_password_cb )
    add_stateful_action( GActionMap( action_group ), "create_room", String, "1", create_room_cb )
    push!( win.win, Gtk4.GLib.GActionGroup( action_group ), "create_room" )
    return
end

end
