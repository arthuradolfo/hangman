module MainMenuActions

using ..Hangman
using ..Hangman.Server
using ..Hangman.WindowModel
using ..Hangman.Words, ..Hangman.PageConstants
using ..Hangman.UI, ..Hangman.KeyEvents, ..Hangman.HighScoreActions

using Gtk4, GtkObservables, Gtk4.GLib, Base.Threads

function create_local_game_options_menu_cb( a, v )::Nothing
    set_current_page( LOCAL_GAME_MENU )
    Gtk4.GLib.set_state( a, v )
    return
end

function start_online_game_cb( a, v )::Nothing
    set_current_page( ONLINE_GAME_MENU )
    Gtk4.GLib.set_state( a, v )
    return
end

function high_score_cb( a, v )::Nothing
    load_highscore( get_highscore() )

    set_current_page( HIGHSCORE_PAGE )
    
    Gtk4.GLib.set_state( a, v )
    return
end

export add_menu_actions
function add_menu_actions( win::Window )::Nothing
    action_group = GSimpleActionGroup()
    add_stateful_action( GActionMap( action_group ), "create_local_game_options_menu", String, "1", create_local_game_options_menu_cb )
    add_stateful_action( GActionMap( action_group ), "create_online_game_options_menu", String, "1", start_online_game_cb )
    add_stateful_action( GActionMap( action_group ), "create_high_score_page", String, "1", high_score_cb )
    push!( win.win, Gtk4.GLib.GActionGroup( action_group ), "menu" )
    unset_key_event()
    return
end

end
