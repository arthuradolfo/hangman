module WaitWordPage

using ..Hangman, ..Hangman.WindowModel
using ..Hangman.UI

using Gtk4, GtkObservables, Gtk4.GLib

export create_wait_word_page
function create_wait_word_page( win::Window )
    box = GtkBox(:v)
    box.homogeneous = false
    
    banner = GtkBox(:v)
    change_css_class(win, banner, "banner")
   
    wait_label_box = GtkCenterBox()

    wait_word_label = GtkButton( "Waiting for word..." )

    wait_label_box[ :center ] = wait_word_label

    push!( box, banner )
    push!( box, wait_label_box )
    push!( win.win, box )
    change_css_class( win, win.win, "menu" )
    return
end

end
