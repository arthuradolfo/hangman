module MainMenu

using ..Hangman, ..Hangman.WindowModel
using ..Hangman.UI

using Gtk4, GtkObservables, Gtk4.GLib

export create_main_menu
function create_main_menu( win::Window )::Nothing
    box = GtkBox(:v)
    box.homogeneous = false

    banner = GtkBox(:v)
    change_css_class(win, banner, "banner")

    buttons_box_new = GtkCenterBox()

    new_game_btn = GtkButton( "New Game" )
    new_game_btn.action_name = "menu.create_local_game_options_menu"
    new_game_btn.action_target = GVariant( "a" )
    change_css_class(win, new_game_btn, "start-btn")

    buttons_box_new[ :center ] = new_game_btn

    buttons_box_wan = GtkCenterBox()

    wan_game_btn = GtkButton( "New Online Game" )
    wan_game_btn.action_name = "menu.create_online_game_options_menu"
    wan_game_btn.action_target = GVariant( "a" )
    change_css_class(win, wan_game_btn, "start-btn")

    buttons_box_wan[ :center ] = wan_game_btn

    buttons_box_score = GtkCenterBox()

    score_btn = GtkButton( "High Score" )
    score_btn.action_name = "menu.create_high_score_page"
    score_btn.action_target = GVariant( "a" )
    change_css_class(win, score_btn, "start-btn")

    buttons_box_score[ :center ] = score_btn

    push!( box, banner )
    push!( box, buttons_box_new )
    push!( box, buttons_box_wan )
    push!( box, buttons_box_score )
    push!( win.win, box )
    change_css_class( win, win.win, "menu" )
    return
end

end
