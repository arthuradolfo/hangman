module HangmanPage

using ..Hangman, ..Hangman.WindowModel, ..Hangman.PropertiesModel, Assets
using ..Hangman.UI

using Gtk4, GtkObservables, Gtk4.GLib

function create_grid( win::Window )::GtkGrid
    grid = GtkGrid()
    line = 1
    column = 1
    for i = 1:26
        btn = GtkButton( string( Char( 96 + i ) ) )
        btn.action_name = "hangman.reveal_letter"
        btn.action_target = GVariant( string( Char( 96 + i ) ) )
        win.btn_list[ Char( 96 + i ) ] = btn
        grid[ column, line ] = btn
        column += 1
        if i % 10 == 0
            line += 1
            column = 1
        end
    end
    change_css_class(win, grid, "btn-grid")
    return grid
end

export create_hangman_page
function create_hangman_page( win::Window )::Nothing
    box = GtkBox( :v )
    forca_image = GtkImage(asset"img/forca.png")
    forca_image.pixel_size = 250    
    push!( box, forca_image )

    change_css_class( win, win.label.widget, "label" )
    push!( box, win.label )

    sep = GtkSeparator( :v )
    sep.margin_top = 30
    sep.margin_bottom = 30
    push!( box, sep )

    grid_box = GtkCenterBox()
    grid_box[ :center ] = create_grid(win)

    push!( box, grid_box )
    push!( win.win, box ) 
    return
end

export create_write_word_page
function create_write_word_page( win::Window )::Nothing
    box = GtkBox(:v)
    box.homogeneous = false
    
    banner = GtkBox(:v)
    change_css_class(win, banner, "banner")
   
    textbox_box = GtkCenterBox()

    word_textbox = GtkText()
    word_textbox.placeholder_text = "Enter the desired word"

    textbox_box[ :center ] = word_textbox

    button_box = GtkCenterBox()

    confirm_word_btn = GtkButton( "Confirm" )
    confirm_word_btn.action_name = "hangman.start_two_game"
    confirm_word_btn.action_target = GVariant( "a" )
    change_css_class(win, confirm_word_btn, "start-btn")

    button_box[ :center ] = confirm_word_btn
   
    push!( box, banner )
    push!( box, textbox_box )
    push!( box, button_box )
    push!( win.win, box )
    change_css_class( win, win.win, "menu" )
    return
end

export create_write_online_word_page
function create_write_online_word_page( win::Window )::Nothing
    box = GtkBox(:v)
    box.homogeneous = false
    
    banner = GtkBox(:v)
    change_css_class(win, banner, "banner")
   
    textbox_box = GtkCenterBox()

    word_textbox = GtkText()
    word_textbox.placeholder_text = "Enter the desired word"

    textbox_box[ :center ] = word_textbox

    button_box = GtkCenterBox()

    confirm_word_btn = GtkButton( "Confirm" )
    confirm_word_btn.action_name = "hangman.start_online_game"
    confirm_word_btn.action_target = GVariant( "a" )
    change_css_class(win, confirm_word_btn, "start-btn")

    button_box[ :center ] = confirm_word_btn
   
    push!( box, banner )
    push!( box, textbox_box )
    push!( box, button_box )
    push!( win.win, box )
    change_css_class( win, win.win, "menu" )
    return
end
export create_ending_page
function create_ending_page( win::Window )::Nothing
    box = GtkBox(:v)
    box.homogeneous = false
   
    label_box = GtkCenterBox()

    label_end = GtkLabel( "YOU LOST!\n\rSCORE: " * string(get_properties().score) )
    change_css_class( win, label_end, "label-end" )

    label_box[ :center ] = label_end

    textbox_box = GtkCenterBox()

    word_textbox = GtkText()
    word_textbox.placeholder_text = "Enter your name"
    change_css_class( win, word_textbox, "username-box" )

    textbox_box[ :center ] = word_textbox

    button_box = GtkCenterBox()

    confirm_word_btn = GtkButton( "Confirm" )
    confirm_word_btn.action_name = "hangman.save_highscore"
    confirm_word_btn.action_target = GVariant( "a" )
    change_css_class(win, confirm_word_btn, "start-btn")

    button_box[ :center ] = confirm_word_btn

    push!( box, label_box )
    push!( box, textbox_box )
    push!( box, button_box )
    push!( win.win, box )
    change_css_class( win, win.win, "menu" )
    return
end

end
