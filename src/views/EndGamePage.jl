module EndGamePage

using ..Hangman, ..Hangman.WindowModel
using ..Hangman.Server, ..Hangman.Client
using ..Hangman.UI

using Gtk4, GtkObservables, Gtk4.GLib

export create_end_game_page
function create_end_game_page( win::Window )::Nothing
    box = GtkBox(:v)
    box.homogeneous = false

    banner = GtkLabel( "Game finished!" )
    change_css_class(win, banner, "label-h1")

    score_list = GtkListStore( String, String, Int64 )

    score_dict = Dict{ UInt64, Int64 }()
    for ( i, ( id, scores ) ) in enumerate( get_client().points )
        sum_points = 0
        for score in scores
            sum_points += score
        end
        score_dict[ id ] = sum_points
    end

    for ( i, ( id, score ) ) in enumerate( 
                sort( collect( score_dict ), by = x -> x.second, rev = true )
               )
        push!( score_list, ( "#" * string( i ),
                            get_client().guests[ id ].name,
                            score
                           ) )
    end

    score_tree = GtkTreeView( GtkTreeModel( score_list ) )
    score_tree.headers_visible = false
    score_tree.halign = Gtk4.Align_BASELINE_CENTER
    change_css_class(win, score_tree, "cell-hs")

    renderer = GtkCellRendererText()

    rank_column = GtkTreeViewColumn(
                                        "Rank",
                                        renderer,
                                        Dict( [ ( "text", 0 ) ] )
                                    )
    rank_column.expand = true

    name_column = GtkTreeViewColumn(
                                        "Name",
                                        renderer,
                                        Dict( [ ( "text", 1 ) ] )
                                    )
    name_column.expand = true

    score_column = GtkTreeViewColumn(
                                        "Score",
                                        renderer,
                                        Dict( [ ( "text", 2 ) ] )
                                    )
    score_column.expand = true

    push!( score_tree, rank_column, name_column, score_column )

    push!( box, banner )
    push!( box, score_tree )
    push!( win.win, box )
    change_css_class( win, win.win, "menu" )
    return
end

end
