module CreateRoomPage

using Gtk4: name
using ..Hangman, ..Hangman.WindowModel
using ..Hangman.UI

using Gtk4, GtkObservables, Gtk4.GLib

export create_create_room_page
function create_create_room_page( win::Window )::Nothing
    box = GtkBox(:v)
    box.homogeneous = false
    
    banner = GtkBox(:v)
    change_css_class(win, banner, "banner")

    name_txt_box = GtkCenterBox()

    name_txt = GtkText()
    name_txt.placeholder_text = "Choose a name"

    name_txt_box[ :center ] = name_txt
   
    password_chk_box = GtkCenterBox()
    
    set_password_checkbox = GtkCheckButton( "Set password" )
    set_password_checkbox.action_name = "create_room.reveal_password"

    password_chk_box[ :center ] = set_password_checkbox

    password_txt_box = GtkCenterBox()

    password_txt = GtkText()
    password_txt.placeholder_text = "Set a password"
    password_txt.visible = false
    password_txt.visibility = false
    change_css_class( win, password_txt, "username-box" )

    password_txt_box[ :center ] = password_txt

    guests_box = GtkCenterBox()

    number_of_guests_cbox = GtkComboBoxText()
    choices = [ "2", "3", "4", "5", "6" ]
    for choice in choices
        push!( number_of_guests_cbox, choice )
    end
    number_of_guests_cbox.active = 0

    guests_box[ :center ] = number_of_guests_cbox

    buttons_box_create = GtkCenterBox()

    create_room_btn = GtkButton( "Create Room" )
    create_room_btn.action_name = "create_room.create_room"
    create_room_btn.action_target = GVariant( "a" )
    change_css_class(win, create_room_btn, "start-btn")

    buttons_box_create[ :center ] = create_room_btn

    push!( box, banner )
    push!( box, name_txt_box )
    push!( box, password_chk_box )
    push!( box, password_txt_box )
    push!( box, guests_box )
    push!( box, buttons_box_create )
    push!( win.win, box )
    change_css_class( win, win.win, "menu" )
    return
end

end
