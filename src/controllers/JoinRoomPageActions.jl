module JoinRoomPageActions

using ..Hangman
using ..Hangman.Server, ..Hangman.Client
using ..Hangman.PageConstants
using ..Hangman.WindowModel, ..Hangman.PropertiesModel
using ..Hangman.JoinRoomPage
using ..Hangman.Words, ..Hangman.PageConstants
using ..Hangman.UI

using Gtk4, GtkObservables, Gtk4.GLib, Base.Threads, Sockets

function join_room_cb( a, v )::Nothing
    name = ""
    ip = ""
    password = ""

    for ( i, child ) in enumerate( get_window().win[] )
        if i == 2
            name = child[ :center ].text
        end
        if i == 3
            ip = child[ :center ].text
        end
        if i == 4
            password = child[ :center ].text
        end
    end

    set_client( HangmanClient(
                    rand( UInt64 ),
                    name,
                    IPv4( ip ),
                    5555,
                    password,
                    LOBBY,
                    TCPSocket(),
                    Dict{ UInt64, Guest }(),
                    "",
                    Dict{ UInt64, Vector{ Int64 } }()
            ) )
    errormonitor( @async start_client( get_client() ) )

    Gtk4.GLib.set_state( a, v )
    return
end


export add_join_room_actions
function add_join_room_actions( win::Window )::Nothing
    action_group = GSimpleActionGroup()
    add_stateful_action( GActionMap( action_group ), "join_room", String, "1", join_room_cb )
    push!( win.win, Gtk4.GLib.GActionGroup( action_group ), "join_room" )
    return
end

end
