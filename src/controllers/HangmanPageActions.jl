module HangmanPageActions

using ..Hangman
using ..Hangman.Server, ..Hangman.Client
using ..Hangman.WindowModel, ..Hangman.PropertiesModel, ..Hangman.HighScoreModel
using ..Hangman.Words, ..Hangman.PageConstants
using ..Hangman.HangmanPage
using ..Hangman.UI, ..Hangman.KeyEvents, ..Hangman.HighScoreActions

using Gtk4, GtkObservables, Gtk4.GLib

function show_lost_page()::Nothing
    unset_key_event()
    set_current_page( HANGMAN_LOST_PAGE )
    reset_lifes( get_properties() )
    return
end

function show_life_lost_page( win::Any )::Nothing
    if get_properties().mode == ONLINE_PLAYER
        send_points( get_client(), get_properties().score )
        return
    end
    set_current_page( HANGMAN_LOSE_PAGE )
    box_end = GtkBox( :v; homogeneous = true )
    label_end = GtkLabel( "YOU LOST 1 LIFE!\nREMAINING: " * string(get_properties().lifes) )
    push!( box_end, label_end )
    push!( win.win, box_end )
    
    unset_key_event()
    set_key_event( reload_hangman_page )
    return
end

function show_win_page( win::Any )::Nothing
    if get_properties().mode == ONLINE_PLAYER
        get_properties().score = 100 - get_properties().mistakes * 20
        send_points( get_client(), get_properties().score )
        reset_mistakes( get_properties() )
        get_properties().score = 0
        return
    end
    set_current_page( HANGMAN_WIN_PAGE )
    box_win = GtkBox( :v; homogeneous = true )
    label_win = GtkLabel("YOU GOT RIGHT! GO FOR THE NEXT WORD!")
    push!( box_win, label_win )
    push!( win.win, box_win )
    
    unset_key_event()
    set_key_event( reload_hangman_page )
    return
end

function reveal_letter( win::Any, onematch::Any, c::String )::Nothing
    win.label[] = win.label[][1:( onematch.offset - 1 )] * 
                    c * win.label[][( onematch.offset + 1 ):end]
    return
end

export update_keyboard
function update_keyboard( key::Char )::Nothing
    matches = check_letter( key, get_properties() )
    if isempty( matches )
        if key in keys( get_window().btn_list )
            change_css_class( get_window(), get_window().btn_list[ key ][], "wrong-btn" )
            get_window().btn_list[ key ][].can_target = false
            increase_mistakes( get_properties() )
            if get_properties().mistakes == MAX_MISTAKES
                reset_mistakes( get_properties() )
                decrease_lifes( get_properties() )
                if get_properties().lifes == 0 && get_properties().mode != ONLINE_PLAYER
                    show_lost_page()
                else
                    show_life_lost_page( get_window() ) 
                end
            end
        end
    else
        change_css_class( get_window(), get_window().btn_list[ key ][], "correct-btn" )
        get_window().btn_list[ key ][].can_target = false
        for onematch in matches
            reveal_letter(get_window(), onematch, string( key ) )
            if isnothing( match( r"_", get_window().label[] ) )
                show_win_page(get_window())
                if get_properties().mode == ONLINE_PLAYER
                    return
                end
                reset_mistakes( get_properties() )
                add_correct_word_score( get_properties() )
            end
        end
    end
    return
end

export reveal_letter_action_cb
function reveal_letter_action_cb( a, v )::Nothing
    update_keyboard( v[ String ][ 1 ] )
    Gtk4.GLib.set_state( a, v )
    return
end

function start_two_game_cb( a, v )::Nothing
    for ( i, child ) in enumerate( get_window().win[] )
        if i == 2
            get_properties().current_word = child[ :center ].text
        end
    end

    get_window().label = label( "" )
    
    get_window().label[] = hide_word( get_properties().current_word )

    set_current_page( HANGMAN_PAGE )
    Gtk4.GLib.set_state( a, v )
    return
end

export start_online_game
function start_online_game()::Nothing
    set_mode( get_properties(), ONLINE_PLAYER )
    for ( i, child ) in enumerate( get_window().win[] )
        if i == 2
            get_properties().current_word = get_client().current_word
        end
    end

    get_window().label = label( "" )
    
    get_window().label[] = hide_word( get_properties().current_word )

    set_current_page( HANGMAN_PAGE )
    return
end

function start_online_game_cb( a, v )::Nothing
    word = ""
    for ( i, child ) in enumerate( get_window().win[] )
        if i == 2
            word = child[ :center ].text
        end
    end
    send_word( get_client(), word )
    return
end

export capture_key
function capture_key( key::Char )::Nothing
    if !( key in get_properties().keys_pressed )
        push!( get_properties().keys_pressed, key )
        update_keyboard( key )
    end
    return 
end

export reload_hangman_page
function reload_hangman_page()::Nothing
    if get_properties().mode == ONE_PLAYER
        get_window().label = label( "" )
        
        generate_word( get_properties() )
        
        get_window().label[] = hide_word( get_properties().current_word )
        
        set_current_page( HANGMAN_PAGE )
    elseif get_properties().mode == TWO_PLAYERS
        set_current_page( WRITE_WORD_PAGE )
    elseif get_properties().mode == ONLINE_PLAYER
        set_current_page( WAIT_WORD )
    end
    return
end

function save_highscore_cb( a, v )::Nothing
    username = ""
    for ( i, child ) in enumerate( get_window().win[] )
        if i == 2
            username = child[ :center ].text
        end
    end
    save_highscore( get_highscore(), get_properties().score, username )
    set_current_page( MAIN_MENU )
    Gtk4.GLib.set_state( a, v )
    return
end


export add_hangman_actions
function add_hangman_actions( win::Window )::Nothing
    action_group = GSimpleActionGroup()
    add_stateful_action( GActionMap( action_group ), "reveal_letter", String, "1", reveal_letter_action_cb )
    add_stateful_action( GActionMap( action_group ), "start_two_game", String, "1", start_two_game_cb )
    add_stateful_action( GActionMap( action_group ), "start_online_game", String, "1", start_online_game_cb )
    add_stateful_action( GActionMap( action_group ), "save_highscore", String, "1", save_highscore_cb )
    push!( win.win, Gtk4.GLib.GActionGroup( action_group ), "hangman" )
    unset_key_event()
    reset_keys_pressed( get_properties() )
    set_key_event_catch_key( capture_key )
    return
end

end
