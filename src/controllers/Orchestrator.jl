module Orchestrator

using ..Hangman
using ..Hangman.WindowModel, ..Hangman.PropertiesModel
using ..Hangman.MainMenu, ..Hangman.HangmanPage, ..Hangman.LocalGameMenu,
        ..Hangman.OnlineGameMenu, ..Hangman.HighScorePage,
        ..Hangman.CreateRoomPage, ..Hangman.LobbyPage, ..Hangman.JoinRoomPage,
        ..Hangman.WaitWordPage, ..Hangman.EndRoundPage, ..Hangman.EndGamePage
using ..Hangman.Words, ..Hangman.PageConstants
using ..Hangman.UI, ..Hangman.MainMenuActions, ..Hangman.LocalGameMenuActions,
        ..Hangman.HangmanPageActions, ..Hangman.HighScoreActions,
        ..Hangman.OnlineGameMenuActions, ..Hangman.CreateRoomPageActions,
        ..Hangman.LobbyPageActions, ..Hangman.JoinRoomPageActions

export setup_page
function setup_page()::Int32
    if get_last_page() == get_current_page()
        return 1
    end

    set_last_page( get_current_page() )

    if get_current_page() == HANGMAN_PAGE
        create_hangman_page( get_window() )
        add_hangman_actions( get_window() )

    elseif get_current_page() == HANGMAN_ONLINE_PAGE
        start_online_game()

    elseif get_current_page() == LOCAL_GAME_MENU
        create_local_game_options_menu( get_window() )
        add_local_game_menu_actions( get_window() )

    elseif get_current_page() == ONLINE_GAME_MENU
        create_online_game_options_menu( get_window() )
        add_online_game_menu_actions( get_window() )

    elseif get_current_page() == MAIN_MENU
        create_main_menu( get_window() )
        add_menu_actions( get_window() )

    elseif get_current_page() == JOIN_ROOM_PAGE
        create_join_room_page( get_window() )
        add_join_room_actions( get_window() )

    elseif get_current_page() == WRITE_WORD_PAGE
        create_write_word_page( get_window() )
        add_hangman_actions( get_window() )

    elseif get_current_page() == WRITE_ONLINE_WORD_PAGE
        create_write_online_word_page( get_window() )
        add_hangman_actions( get_window() )

    elseif get_current_page() == WAIT_WORD_PAGE
        create_wait_word_page( get_window() )

    elseif get_current_page() == END_ROUND_PAGE
        create_end_round_page( get_window() )

    elseif get_current_page() == END_GAME_PAGE
        create_end_game_page( get_window() )

    elseif get_current_page() == HANGMAN_LOST_PAGE
        create_ending_page( get_window() )
        add_hangman_actions( get_window() )

    elseif get_current_page() == CREATE_ROOM_PAGE
        create_create_room_page( get_window() )
        add_create_room_actions( get_window() )

    elseif get_current_page() == LOBBY_PAGE
        create_lobby_page( get_window() )
        add_lobby_page_actions( get_window() )

    elseif get_current_page() == HIGHSCORE_PAGE
        create_highscore_page( get_window() )
    end
    return 1
end

end
