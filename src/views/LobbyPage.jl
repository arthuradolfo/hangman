module LobbyPage

using ..Hangman, ..Hangman.WindowModel
using ..Hangman.Server
using ..Hangman.UI

using Gtk4, GtkObservables, Gtk4.GLib

guest_list = Ref{ GtkListStore }()

export add_guest
function add_guest( guest::String )
    push!( guest_list[], ( guest, ) )
end

export create_lobby_page
function create_lobby_page( win::Window )::Nothing
    box = GtkBox(:v)
    box.homogeneous = false

    banner = GtkLabel( "Lobby" )
    change_css_class(win, banner, "label-h1")

    guest_list[] = GtkListStore( String )

    guests_tree = GtkTreeView( GtkTreeModel( guest_list[] ) )
    guests_tree.headers_visible = false
    guests_tree.halign = Gtk4.Align_BASELINE_CENTER
    change_css_class(win, guests_tree, "cell-hs")

    renderer = GtkCellRendererText()

    name_column = GtkTreeViewColumn(
                                        "Name",
                                        renderer,
                                        Dict( [ ( "text", 0 ) ] )
                                    )
    name_column.expand = true

    push!( guests_tree, name_column )

    btn_box = GtkCenterBox()

    btn_start = GtkButton( "Start Game" )
    btn_start.action_name = "lobby.start_online_game"

    btn_box[ :center ] = btn_start

    push!( box, banner )
    push!( box, guests_tree )
    if isserverdefined()
        push!( box, btn_box )
    end
    push!( win.win, box )
    change_css_class( win, win.win, "menu" )
    return
end

end
