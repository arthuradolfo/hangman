module OnlineGameMenuActions

using ..Hangman
using ..Hangman.WindowModel, ..Hangman.PropertiesModel
using ..Hangman.HangmanPage
using ..Hangman.Words, ..Hangman.PageConstants
using ..Hangman.UI

using Gtk4, GtkObservables, Gtk4.GLib

function create_room_page_cb( a, v )::Nothing
    set_current_page( CREATE_ROOM_PAGE )
    Gtk4.GLib.set_state( a, v )
    return
end

function join_room_page_cb( a, v )::Nothing
    set_current_page( JOIN_ROOM_PAGE )
    Gtk4.GLib.set_state( a, v )
    return
end


export add_online_game_menu_actions
function add_online_game_menu_actions( win::Window )::Nothing
    action_group = GSimpleActionGroup()
    add_stateful_action( GActionMap( action_group ), "create_room_page", String, "1", create_room_page_cb )
    add_stateful_action( GActionMap( action_group ), "join_room_page", String, "1", join_room_page_cb )
    push!( win.win, Gtk4.GLib.GActionGroup( action_group ), "online_game_menu" )
    return
end

end
