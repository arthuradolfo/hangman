module Words

using ..Hangman, Assets
using ..Hangman.PropertiesModel

export read_words_from_file
function read_words_from_file()::Vector{Any}
    words = []
    open(asset"words/words.txt") do file_content
        while ! eof(file_content)
            word = readline(file_content)
            push!(words, word)
        end
    end 
    words
end

export hide_word
function hide_word( a::String )::String
    replace( a, r"\w" => '_' )
end

export generate_word
function generate_word(props)::Nothing
    props.current_word = rand( props.dictionaire )
    return
end

export check_letter
function check_letter( c::Char, props::Properties )::Vector{RegexMatch}
    collect( eachmatch( Regex( "$c" ), props.current_word ) )
end

end
