module LobbyPageActions

using ..Hangman
using ..Hangman.Server, ..Hangman.Client
using ..Hangman.WindowModel, ..Hangman.PropertiesModel
using ..Hangman.LobbyPage
using ..Hangman.Words, ..Hangman.PageConstants
using ..Hangman.UI

using Gtk4, GtkObservables, Gtk4.GLib

function check_new_guests()::Int32
    if isserverdefined()
        for ( _, guest ) in get_server().guests
            if !guest.ready
                add_guest( guest.name )
                guest.ready = true
            end
        end
    elseif isclientdefined()
        request_guests( get_client().conn )
        print( get_client().guests )
        for ( _, guest ) in get_client().guests
            if !guest.ready
                add_guest( guest.name )
                guest.ready = true
            end
        end
    end
    return 1
end

function start_online_game_cb( a, v )::Nothing
    start_server_game( get_server() )

    Gtk4.GLib.set_state( a, v )
    return
end

export add_lobby_page_actions
function add_lobby_page_actions( win::Window )::Nothing
    g_timeout_add( check_new_guests, 1000 )
    action_group = GSimpleActionGroup()
    add_stateful_action( GActionMap( action_group ), "start_online_game", false, start_online_game_cb )
    push!( win.win, Gtk4.GLib.GActionGroup( action_group ), "lobby" )
    return
end

end
