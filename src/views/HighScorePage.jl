module HighScorePage

using ..Hangman, ..Hangman.WindowModel
using ..Hangman.UI, ..Hangman.HighScoreActions

using Gtk4, GtkObservables, Gtk4.GLib

export create_highscore_page
function create_highscore_page( win::Window )::Nothing
    box = GtkBox(:v)
    box.homogeneous = false

    banner = GtkLabel( "High Scores" )
    change_css_class(win, banner, "label-h1")

    list = GtkListStore( String, Int, String )
    for ( i, row ) in enumerate( eachrow( get_highscore().scores ) )
        push!( list, ( "#" * string( i ), row.score, row.username ) )
    end

    highscore_tree = GtkTreeView( GtkTreeModel( list ) )
    highscore_tree.headers_visible = false
    highscore_tree.halign = Gtk4.Align_BASELINE_CENTER
    change_css_class(win, highscore_tree, "cell-hs")

    renderer = GtkCellRendererText()

    ranking_column = GtkTreeViewColumn(
                                        "#Rank",
                                        renderer,
                                        Dict( [ ( "text", 0 ) ] )
                                    )
    ranking_column.expand = true

    score_column = GtkTreeViewColumn(
                                        "Score",
                                        renderer,
                                        Dict( [ ( "text", 1 ) ] )
                                    )
    score_column.expand = true
    
    username_column = GtkTreeViewColumn(
                                        "Name",
                                        renderer,
                                        Dict( [ ( "text", 2 ) ] )
                                    )
    username_column.expand = true

    push!( highscore_tree, ranking_column, score_column, username_column )

    push!( box, banner )
    push!( box, highscore_tree )
    push!( win.win, box )
    change_css_class( win, win.win, "menu" )
    return
end

end
