module JoinRoomPage

using Gtk4: name
using ..Hangman, ..Hangman.WindowModel
using ..Hangman.UI

using Gtk4, GtkObservables, Gtk4.GLib

export create_join_room_page
function create_join_room_page( win::Window )::Nothing
    box = GtkBox(:v)
    box.homogeneous = false
    
    banner = GtkBox(:v)
    change_css_class(win, banner, "banner")

    name_txt_box = GtkCenterBox()

    name_txt = GtkText()
    name_txt.placeholder_text = "Choose a name"
    change_css_class( win, name_txt, "username-box" )

    name_txt_box[ :center ] = name_txt

    ip_txt_box = GtkCenterBox()

    ip_txt = GtkText()
    ip_txt.placeholder_text = "Room IP"
    change_css_class( win, ip_txt, "username-box" )

    ip_txt_box[ :center ] = ip_txt
   
    password_txt_box = GtkCenterBox()

    password_txt = GtkText()
    password_txt.placeholder_text = "Room password"
    change_css_class( win, password_txt, "username-box" )

    password_txt_box[ :center ] = password_txt

    buttons_box_join = GtkCenterBox()

    join_room_btn = GtkButton( "Join Room" )
    join_room_btn.action_name = "join_room.join_room"
    join_room_btn.action_target = GVariant( "a" )
    change_css_class(win, join_room_btn, "start-btn")

    buttons_box_join[ :center ] = join_room_btn

    push!( box, banner )
    push!( box, name_txt_box )
    push!( box, ip_txt_box )
    push!( box, password_txt_box )
    push!( box, buttons_box_join )
    push!( win.win, box )
    change_css_class( win, win.win, "menu" )
    return
end

end
