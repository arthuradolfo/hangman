module Database

using SQLite
using DataFrames

export execute_sql_file
function execute_sql_file( db_name::String, sql_name::String )
    db = SQLite.DB( db_name )
    
    sql = ""
    open( sql_name ) do file_content
        while ! eof(file_content)
            line = readline(file_content)
            sql *= line * "\n"
        end
    end

    return SQLite.execute( db, sql )
end

export execute_insert
function execute_insert( db_name::String, sql::String, params::NamedTuple )
    db = SQLite.DB( db_name )
    return SQLite.execute( db, sql, params )
end

export execute_select
function execute_select( db_name::String, sql::String )::DataFrame
    db = SQLite.DB( db_name )
    return DBInterface.execute( db, sql ) |> DataFrame
end

end
