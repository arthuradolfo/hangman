module UI

using ..Hangman
using ..Hangman.Server, ..Hangman.Client
using ..Hangman.WindowModel, ..Hangman.PropertiesModel
using Assets

using Gtk4, GtkObservables, Gtk4.GLib

gwin = Ref{ Window }()
gprops = Ref{ Properties }()
gserver = Ref{ HangmanServer }()
gclient = Ref{ HangmanClient }()

export set_window
function set_window( win::Window )::Nothing
    gwin[] = win
    return
end

export get_window
function get_window()::Window
    gwin[]
end

export set_properties
function set_properties( props::Properties )::Nothing
    gprops[] = props
    return
end

export get_properties
function get_properties()::Properties
    gprops[]
end

export isserverdefined
function isserverdefined()::Bool
    return isdefined( gserver, 1 )
end

export isclientdefined
function isclientdefined()::Bool
    return isdefined( gclient, 1 )
end

export set_server
function set_server( server::HangmanServer )::Nothing
    gserver[] = server
    return
end

export get_server
function get_server()::HangmanServer
    return gserver[]
end

export set_client
function set_client( client::HangmanClient )::Nothing
    gclient[] = client
    return
end

export get_client
function get_client()::HangmanClient
    return gclient[]
end

export change_css_class
function change_css_class( 
        win::Window, 
        widget::GtkWidget, 
        class_name::String 
)::Nothing
    add_css_class( widget, class_name )
    context = Gtk4.G_.get_style_context( widget )
    Gtk4.G_.add_provider( 
        context, 
        GtkStyleProvider( win.css_provider ), 
        Gtk4.STYLE_PROVIDER_PRIORITY_USER 
    )
    Gtk4.G_.save( context )
    return
end

export load_css_classes
function load_css_classes( win::Window )::Nothing
    Gtk4.G_.load_from_path( win.css_provider, asset"css/style.css" )
    return
end

export wait_for_signal
function wait_for_signal( win::Window )::Nothing
    if !isinteractive()
        c = Condition()
        signal_connect( win.win, :close_request ) do widget
            notify( c )
        end
        @async Gtk4.GLib.glib_main()
        wait( c )
    end
    return
end

end
