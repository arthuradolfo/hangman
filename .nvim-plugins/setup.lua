vim.g.main_window_id = vim.fn.win_getid()

vim.cmd([[NERDTreeToggle]])
vim.g.tree_window_id = vim.fn.win_getid()

vim.fn.win_gotoid(vim.g.main_window_id)

vim.cmd([[belowright 15split +term]])
vim.g.terminal_window_id = vim.fn.win_getid()

vim.keymap.set('n', '<leader>c', function()
	vim.fn.win_gotoid(vim.g.terminal_window_id)
	vim.api.nvim_input('ijulia -q --project build.jl<cr><esc>')
end)

vim.keymap.set('n', '<leader>r', function()
	vim.fn.win_gotoid(vim.g.terminal_window_id)
	vim.api.nvim_input('i../hangman_compiled/bin/Hangman<cr><esc>')
end)

vim.keymap.set('n', '<leader>t', function()
	vim.fn.win_gotoid(vim.g.terminal_window_id)
	vim.api.nvim_input('i<ctrl+C><esc>')
end)

vim.keymap.set('n', '<leader>d', function()
	vim.fn.win_gotoid(vim.g.terminal_window_id)
	vim.api.nvim_input('ijulia -q --project<cr>include("debug.jl")<cr>')
end)

vim.keymap.set('n', '<leader>x', function()
	vim.fn.win_gotoid(vim.g.terminal_window_id)
	vim.api.nvim_input('ijulia -q --project<cr>include("src/Hangman.jl")<cr>Hangman.julia_main()<cr>')
end)
