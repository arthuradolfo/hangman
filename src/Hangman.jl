module Hangman

using Gtk4, GtkObservables, Gtk4.GLib
using DataFrames

include( "constants/PageConstants.jl" )
include( "backend/Database.jl" )
include( "backend/Server.jl" )
include( "backend/Client.jl" )
include( "models/HighScore.jl" )
include( "models/Window.jl" )
include( "models/Properties.jl" )
include( "controllers/HighScoreActions.jl" )
include( "controllers/UI.jl" )
include( "views/MainMenu.jl" )
include( "views/HighScorePage.jl" )
include( "views/OnlineGameMenu.jl" )
include( "views/JoinRoomPage.jl" )
include( "views/WaitWordPage.jl" )
include( "views/LobbyPage.jl" )
include( "views/CreateRoomPage.jl" )
include( "views/LocalGameMenu.jl" )
include( "views/EndRoundPage.jl" )
include( "views/EndGamePage.jl" )
include( "views/HangmanPage.jl" )
include( "backend/Words.jl" )
include( "controllers/KeyEvents.jl" )
include( "controllers/HangmanPageActions.jl" )
include( "controllers/CreateRoomPageActions.jl" )
include( "controllers/OnlineGameMenuActions.jl" )
include( "controllers/JoinRoomPageActions.jl" )
include( "controllers/LobbyPageActions.jl" )
include( "controllers/LocalGameMenuActions.jl" )
include( "controllers/MainMenuActions.jl" )
include( "controllers/Orchestrator.jl" )

using Assets
using .Database
using .WindowModel, .PropertiesModel, .HighScoreModel
using .Words
using .PageConstants
using .UI, .Orchestrator, .HighScoreActions

function setup_highscore()::Nothing
    highscore = HighScore(
                          DataFrame(),
                          asset"db/highscore.db"
    )
    set_highscore( highscore )
    initialize_highscore_db( highscore )
end

function setup_properties()::Nothing
    props = Properties(
                        read_words_from_file(),
                        "",
                        5,
                        0,
                        0,
                        ONE_PLAYER,
                        Vector{Char}()
    )
    set_properties( props )
    return
end

function setup_main_window()::Nothing
    win_gtk = GtkWindow( "Forca" )
    
    win = Window( 
                win_gtk, 
                label( "" ),
                Dict{ Char, Ref{ GtkButton } }(),
                GtkCssProvider(),
                Dict{ String, GtkEventControllerKeyLeaf }(),
                Dict{ String, UInt64 }()
    )
    win.win.default_width = 500
    win.win.default_height = 700
    load_css_classes( win )
    set_window( win )
    return
end

function julia_main()::Cint
    setup_highscore()

    setup_properties()
    
    setup_main_window()
    
    set_current_page( MAIN_MENU )

    setup_page()

    g_timeout_add( setup_page, 300 )

    wait_for_signal( get_window() )

    return 0
end

end 
