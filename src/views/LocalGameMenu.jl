module LocalGameMenu

using ..Hangman, ..Hangman.WindowModel
using ..Hangman.UI

using Gtk4, GtkObservables, Gtk4.GLib

export create_local_game_options_menu
function create_local_game_options_menu( win::Window )::Nothing
    box = GtkBox(:v)
    box.homogeneous = false
    
    banner = GtkBox(:v)
    change_css_class(win, banner, "banner")
   
    buttons_box_one = GtkCenterBox()

    one_player_btn = GtkButton( "One Player" )
    one_player_btn.action_name = "new_game_menu.start_one_new_game"
    one_player_btn.action_target = GVariant( "a" )
    change_css_class(win, one_player_btn, "start-btn")

    buttons_box_one[ :center ] = one_player_btn

    buttons_box_two = GtkCenterBox()

    two_player_btn = GtkButton( "Two Player" )
    two_player_btn.action_name = "new_game_menu.start_two_new_game"
    two_player_btn.action_target = GVariant( "a" )
    change_css_class(win, two_player_btn, "start-btn")

    buttons_box_two[ :center ] = two_player_btn
   
    push!( box, banner )
    push!( box, buttons_box_one )
    push!( box, buttons_box_two )
    push!( win.win, box )
    change_css_class( win, win.win, "menu" )
    return
end

end
