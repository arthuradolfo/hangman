module KeyEvents

using ..Hangman.PageConstants
using ..Hangman.UI
using Gtk4, Gtk4.GLib

export set_key_event
function set_key_event( f::Function, args... )::Nothing
    eck = GtkEventControllerKey(get_window().win)
    id = signal_connect(eck, "key-pressed") do controller, keyval, keycode, state
        f( args... )
    end

    get_window().eck_list[ "win" ] = eck
    get_window().signal_list[ "keypress" ] = id
    return
end

export set_key_event_catch_key
function set_key_event_catch_key( f::Function, args... )::Nothing
    eck = GtkEventControllerKey(get_window().win)
    id = signal_connect(eck, "key-pressed") do controller, keyval, keycode, state
        f( Char( keyval ), args... )
    end

    get_window().eck_list[ "win" ] = eck
    get_window().signal_list[ "keypress" ] = id
    return
end

export unset_key_event
function unset_key_event()::Nothing
    if "win" in keys( get_window().eck_list )
        signal_handler_block(
                            get_window().eck_list[ "win" ],
                            get_window().signal_list[ "keypress" ]
        )
    end
    return
end

end
