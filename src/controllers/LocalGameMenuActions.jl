module LocalGameMenuActions

using ..Hangman
using ..Hangman.WindowModel, ..Hangman.PropertiesModel
using ..Hangman.HangmanPage
using ..Hangman.Words, ..Hangman.PageConstants
using ..Hangman.UI

using Gtk4, GtkObservables, Gtk4.GLib

function start_one_new_game_cb( a, v )::Nothing
    set_mode( get_properties(), ONE_PLAYER )

    get_window().label = label( "" )

    generate_word( get_properties() )
    
    get_window().label[] = hide_word( get_properties().current_word )

    set_current_page( HANGMAN_PAGE )

    Gtk4.GLib.set_state( a, v )
    return
end

function start_two_new_game_cb( a, v )::Nothing
    set_mode( get_properties(), TWO_PLAYERS )
    set_current_page( WRITE_WORD_PAGE )
    Gtk4.GLib.set_state( a, v )
    return
end


export add_local_game_menu_actions
function add_local_game_menu_actions( win::Window )::Nothing
    action_group = GSimpleActionGroup()
    add_stateful_action( GActionMap( action_group ), "start_one_new_game", String, "1", start_one_new_game_cb )
    add_stateful_action( GActionMap( action_group ), "start_two_new_game", String, "1", start_two_new_game_cb )
    push!( win.win, Gtk4.GLib.GActionGroup( action_group ), "new_game_menu" )
    return
end

end
