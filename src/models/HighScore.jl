module HighScoreModel

using DataFrames

export HighScore
mutable struct HighScore
    scores::DataFrame
    file_name::String
end

end
