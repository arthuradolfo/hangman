module HighScoreActions

using Assets
using ..Hangman.HighScoreModel
using ..Hangman.Database

insert_sql = "INSERT INTO `highscore` ( score, username ) VALUES ( :score, :username )"
select_sql = "SELECT * FROM `highscore` ORDER BY score DESC"

highscore = Ref{ HighScore }()

export get_highscore
function get_highscore()::HighScore
    return highscore[]
end

export set_highscore
function set_highscore( hs::HighScore )::Nothing
    highscore[] = hs
    return
end

export initialize_highscore_db
function initialize_highscore_db( hs::HighScore )::Nothing
    if !isfile( hs.file_name )
        execute_sql_file( hs.file_name, asset"sql/highscore.sql" )
    end
    return
end

export save_highscore
function save_highscore( hs::HighScore, score::Int64, username::String )::Int64
    return execute_insert( hs.file_name, insert_sql, ( score = score, username = username ) )
end

export load_highscore
function load_highscore( hs::HighScore )
    get_highscore().scores = execute_select( hs.file_name, select_sql )
end

end
